<?php

namespace Drupal\seo_ai\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Stores metadata settings.
 */
class SeoAiConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'seo_ai.settings',
    ];
  }

  /**
   * Constructs a SeoAiConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'seo_ai_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('seo_ai.settings');

    $form['openai'] = [
      '#type' => 'details',
      '#title' => $this->t('OpenAI Settings'),
      '#open' => $config->get('seo_ai.service') == 'openai' ? TRUE : FALSE,
    ];

    $form['openai']['seoai_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#description' => $this->t('Please provide the OpenAI API Endpoint'),
      '#default_value' => $config->get('seo_ai.seoai_endpoint') ?: 'https://api.openai.com/v1/chat/completions',
    ];

    $form['openai']['seoai_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Access Token'),
      '#description' => $this->t('Please provide the OpenAI Access Token'),
      '#default_value' => $config->get('seo_ai.seoai_token'),
    ];

    $form['openai']['seoai_model'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Model Name'),
      '#description' => $this->t('Please provide the OpenAI API Model name'),
      '#default_value' => $config->get('seo_ai.seoai_model') ?: 'gpt-3.5-turbo',
    ];

    $form['openai']['seoai_max_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Max Token'),
      '#description' => $this->t('Please provide the Max token here to limit the output words. Max token is the limit of tokens combining both input prompt and output text.'),
      '#default_value' => $config->get('seo_ai.seoai_max_token') ?: 256,
    ];

    $form['openai']['seoai_max_context_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Max Context Length'),
      '#description' => $this->t('Please provide the Maximum context length to limit the input prompt.'),
      '#default_value' => $config->get('seo_ai.seoai_max_context_length') ?: 4096,
    ];

    $form['openai']['seoai_temperature'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Temperature'),
      '#description' => $this->t('Please provide the OpenAI Temperature value'),
      '#default_value' => $config->get('seo_ai.seoai_temperature') ?: 1,
    ];

    $selected_content_types = $config->get('seo_ai.metadata_content_types');
    $form['notification_instructions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Instructions'),
      '#markup' => $this->t('Select the content type(s) for which you want the SEO AI features to be used.'),
    ];
    $content_types = $this->entityTypeManager
      ->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($content_types as $content_type) {
      $options[$content_type->id()] = $content_type->label();
    }
    $form['metadata_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#options' => $options,
      '#default_value' => $selected_content_types ? $selected_content_types : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $access_token = $form_state->getValue('seoai_token');
    $api_max_token = $form_state->getValue('seoai_max_token');
    $seoai_temperature = $form_state->getValue('seoai_temperature');
    $context_length = $form_state->getValue('seoai_max_context_length');

    if (!empty($access_token) && !preg_match('/^[A-Za-z0-9-_]+$/', $access_token)) {
      $form_state->setErrorByName('seoai_token', $this->t('Access Token contains invalid characters. Only alphanumeric characters, hyphens, and underscores are allowed.'));
    }

    if (!empty($api_max_token) && !is_numeric($api_max_token)) {
      $form_state->setErrorByName('seoai_max_token', $this->t('OpenAI API Max Token must be a number.'));
    }

    if (!empty($seoai_temperature) && !is_numeric($seoai_temperature)) {
      $form_state->setErrorByName('seoai_temperature', $this->t('OpenAI Temperature must be a number.'));
    }

    if (!empty($context_length) && !is_numeric($context_length)) {
      $form_state->setErrorByName('seoai_max_context_length', $this->t('OpenAI Max Context Length must be a number.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $checked_values = $form_state->getValue('metadata_content_types');

    $this->config('seo_ai.settings')
      ->set('seo_ai.seoai_endpoint', $values['seoai_endpoint'])
      ->set('seo_ai.seoai_model', $values['seoai_model'])
      ->set('seo_ai.seoai_max_token', $values['seoai_max_token'])
      ->set('seo_ai.seoai_temperature', $values['seoai_temperature'])
      ->set('seo_ai.seoai_token', $values['seoai_token'])
      ->set('seo_ai.seoai_max_context_length', $values['seoai_max_context_length'])
      ->set('seo_ai.metadata_content_types', $checked_values)
      ->save();

  }

}
