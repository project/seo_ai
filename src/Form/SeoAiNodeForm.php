<?php

namespace Drupal\seo_ai\Form;

use Drupal\node\NodeForm;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\seo_ai\Services\OpenAiMetatagGenerator;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extends the node form.
 */
class SeoAiNodeForm extends NodeForm {
  use StringTranslationTrait;

  /**
   * Seo ai metatag generator.
   */
  protected OpenAiMetatagGenerator $openAiMetatag;

  /**
   * Constructs a new SeoAiNodeForm object.
   */
  public function __construct(
    OpenAiMetatagGenerator $metatagAi,
    ...$default) {
    parent::__construct(...$default);
    $this->openAiMetatag = $metatagAi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('seo_ai.openai_metatag'),
      $container->get('entity.repository'),
      $container->get('tempstore.private'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Call the parent buildForm method to get the original form.
    $form = parent::buildForm($form, $form_state);
    $node = $this->entity;
    $bundle = $node->bundle();
    $config = $this->config('seo_ai.settings');
    $machineName = NULL;
    $selected_content_types = $config->get('seo_ai.metadata_content_types');

    if (!empty($selected_content_types) && in_array($bundle, $selected_content_types)) {
      // Check if content type has metatags field.
      $hasMetatags = FALSE;
      $field_definitions = $node->getFieldDefinitions();
      foreach ($field_definitions as $field_name => $field_definition) {
        if ($field_definition->getType() === 'metatag_computed') {
          $hasMetatags = TRUE;
          $machineName = 'field_' . $field_name;
        }
      }
      if ($hasMetatags) {
        $form['actions']['generate_metadata'] = [
          '#type' => 'button',
          '#value' => $this->t('Generate Metatada'),
          '#weight' => 4,
          '#ajax' => [
            'callback' => [$this, 'submitAiGenerate'],
            'event' => 'click',
            'submit' => [
              'machine_name' => $machineName,
            ],
          ],
        ];
      }

    }

    return $form;
  }

  /**
   * Callback method to generate metadata.
   *
   * @param array $form
   *   Nested array of form elements that comprise the entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax data in the response.
   */
  public function submitAiGenerate(array $form, FormStateInterface $form_state) : AjaxResponse {
    $response = new AjaxResponse();
    $data = $form_state->getValues();
    $metadata_field_id = $form_state->getTriggeringElement()['#ajax']['submit']['machine_name'];
    $elem_id = strtr($metadata_field_id, '_', '-');

    if (!isset($data[$metadata_field_id])) {
      $response->addCommand(new MessageCommand('Metatag field is missing.', NULL, ['type' => 'error'], TRUE));
      return $response;
    }

    $text = '';
    if (isset($data['title']) && isset($data['title'][0]['value'])) {
      $text .= $data['title'][0]['value'];
    }

    if (isset($data['body']) && isset($data['body'][0]['value'])) {
      $text .= ' ' . $data['body'][0]['value'];
    }

    $text = trim($text);
    if (!$text) {
      $response->addCommand(new MessageCommand('Please provide a title and description before proceeding. Unable to process with empty title and description.', NULL, ['type' => 'error'], TRUE));
      return $response;
    }

    $generator = $this->openAiMetatag;
    $result = $generator->generate($text);
    if (!empty($result['hasError'])) {
      foreach ($result as $k => $errMessage) {
        if ($k == 'hasError') {
          continue;
        }
        $response->addCommand(new MessageCommand($errMessage, NULL, ['type' => 'error'], TRUE));
      }
      return $response;
    }

    // Metatag fields to replace.
    $metaNames = [
      'title' => '#edit-' . $elem_id . '-0-basic-title',
      'description' => '#edit-' . $elem_id . '-0-basic-description',
      'abstract' => '#edit-' . $elem_id . '-0-basic-abstract',
      'keywords' => '#edit-' . $elem_id . '-0-basic-keywords',
      'og:title' => '#edit-' . $elem_id . '-0-open-graph-og-title',
      'og:description' => '#edit-' . $elem_id . '-0-open-graph-og-description',
    ];

    foreach ($metaNames as $k => $v) {
      if (!empty($result[$k])) {
        $response->addCommand(new InvokeCommand($v, 'val', [$result[$k]]));
      }
    }

    return $response;
  }

}
