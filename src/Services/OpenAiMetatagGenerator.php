<?php

namespace Drupal\seo_ai\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Main class that generates the metatag using OpenAI.
 */
class OpenAiMetatagGenerator {
  use LoggerChannelTrait;

  /**
   * Module prefix.
   */
  const MODULE_PREFIX = 'seo_ai';

  /**
   * Stores Drupal configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Stores api errors.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * Constructor method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
  ) {
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get(self::MODULE_PREFIX . '.settings');
  }

  /**
   * Call OpenAI api to generate the text.
   */
  public function generate(string $text) {
    $response_body = $this->invokeApi($text);
    if (!empty($this->errors)) {
      $this->errors[] = $this->t('AI generator returned an empty response.');
      $this->errors['hasError'] = TRUE;
      return $this->errors;
    }

    // Extract title and description.
    $response_text = json_decode($response_body->choices[0]->message->content);
    $title = $description = $abstract = $keywords = '';

    if (isset($response_text->title)) {
      $title = $response_text->title;
    }

    if (isset($response_text->description)) {
      $description = $response_text->description;
    }

    if (isset($response_text->abstract)) {
      $abstract = $response_text->abstract;
    }

    if (isset($response_text->keywords)) {
      $keywords = $response_text->keywords;
    }

    if (isset($response_text->OG_TITLE)) {
      $ogTitle = $response_text->OG_TITLE;
    }

    if (isset($response_text->OG_DESCRIPTION)) {
      $ogDescription = $response_text->OG_DESCRIPTION;
    }

    return [
      'title' => $title ?? NULL,
      'description' => $description ?? NULL,
      'abstract' => $abstract ?? NULL,
      'keywords' => $keywords ?? NULL,
      'og:title' => $ogTitle ?? NULL,
      'og:description' => $ogDescription ?? NULL,
    ];
  }

  /**
   * Accepts and returns the result in array.
   *
   * @param string $text
   *   The text to be processed.
   *
   * @return array
   *   Array with title, description, abstract, and keywords needed by Metatag.
   */
  public function invokeApi(string $text) {
    $api_key = $this->config->get(self::MODULE_PREFIX . '.seoai_token');

    $endpoint = $this->config->get(self::MODULE_PREFIX . '.seoai_endpoint');
    $max_tokens = (int) $this->config->get(self::MODULE_PREFIX . '.seoai_max_token');
    $max_context_length = (int) $this->config->get(self::MODULE_PREFIX . '.seoai_max_context_length');
    $model = $this->config->get(self::MODULE_PREFIX . '.seoai_model');
    $temperature = (float) $this->config->get(self::MODULE_PREFIX . '.seoai_temperature');

    if (!$endpoint || !$api_key || !$temperature || !$max_tokens || !$model) {
      if (!$endpoint) {
        $this->errors[] = $this->t('Incomplete Open AI credentials: Missing endpoint.');
      }
      if (!$api_key) {
        $this->errors[] = $this->t('Incomplete Open AI credentials: Missing API Key.');
      }
      if (!$temperature) {
        $this->errors[] = $this->t('Incomplete Open AI credentials: Missing temperature.');
      }
      if (!$max_tokens) {
        $this->errors[] = $this->t('Incomplete Open AI credentials: Missing max token.');
      }
      if (!$model) {
        $this->errors[] = $this->t('Incomplete Open AI credentials: Missing model.');
      }
      return $this->errors;
    }

    $text = substr($text, 0, $max_context_length);

    $message[] = [
      'role' => 'system',
      'content' => 'When i ask for help, you will reply the following information:
        1. title - Focus on the main topic an generate a short (up to 60 characters) title that can have a high CTR in SERP 2
        2. description - Focus on the main topic and generate a comprehensive meta description that can impact positively SEO and is helpful for the user. No longer than 140 characters
        3. abstract - a brief and concise summary of the content with maximum of 150 characters
        4. keywords - maximum of 10 keywords
        5. OG_TITLE - maximum of 60 characters, it should be short and clickbait
        6. OG_DESCRIPTION - maximum of 140 characters, expand the title so you raise the interest of a passive user
        Suggest content for SEO ranking. Reply in JSON format of the title, description, abstract, OG:TITLE, OG:DESCRIPTION and keywords',
    ];
    $message[] = [
      "role" => "user",
      "content" => $text,
    ];

    $body = [
      'model' => $model,
      'messages' => $message,
      'temperature' => $temperature,
      'max_tokens' => $max_tokens,
      "top_p" => 1,
      "frequency_penalty" => 0,
      "presence_penalty" => 0,
    ];

    $data = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $api_key,
      ],
      'body' => json_encode($body),
    ];

    try {
      $client = new Client();
      $response = $client->post($endpoint, $data);
      $contents = $response->getBody()->getContents();

      if (!$this->isJson($contents)) {
        $this->errors[] = $this->t('AI generator returned an invalid format', 'Openapi returned an invalid format' . $contents);
        return $this->errors;
      }

      return json_decode($contents);
    }
    catch (GuzzleException $exception) {
      $response = $exception->getResponse();
      $response_body = $response->getBody()->getContents();
      if ($this->isJson($response_body)) {
        $error = json_decode($response_body);
        $this->errors[] = $this->t('Error @error', [
          '@error' => $error->error->message,
        ]);
      }
      else {
        $this->errors[] = $this->t('Error @error', [
          '@error' => (string) $response_body,
        ]);
      }
    }

    return $this->errors;
  }

  /**
   * Returns boolean.
   */
  private function isJson($string) {
    return is_string($string) && is_array(json_decode($string, TRUE)) && (json_last_error() == JSON_ERROR_NONE) ? TRUE : FALSE;
  }

}
